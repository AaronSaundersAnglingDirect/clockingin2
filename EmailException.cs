﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ClockingInDataBase
{
    internal class EmailException
    {
        private static String _errorlineNo,
            _errormsg,
            _errorLocation,
            _extype,
            _sub,
            _emailHead,
            _emailSing;

        public static void SendErrorTomail(Exception exmail)
        {

            try
            {
                const string newline = "<br/>";
                _errorlineNo = exmail.StackTrace;
                _errormsg = exmail.GetType().Name;
                _extype = exmail.GetType().ToString();
                _errorLocation = exmail.Message;
                _emailHead = "<b>Dear Me,</b>" + "<br/>" + "An exception occurred in a Application Url" + " " +
                            " " + "With following Details" + "<br/>" + "<br/>";
                _emailSing = newline + "Thanks and Regards" + newline + "    " + "     " + "<b>Yourself </b>" + "</br>";
                _sub = "Exception occurred" + " " + "in Application" + " ";
                string errortomail = _emailHead + "<b>Log Written Date: </b>" + " " + DateTime.Now + newline +
                                     "<b>Error Line No :</b>" + " " + _errorlineNo + "\t\n" + " " + newline +
                                     "<b>Error Message:</b>" + " " + _errormsg + newline + "<b>Exception Type:</b>" +
                                     " " + _extype + newline + "<b> Error Details :</b>" + " " + _errorLocation +
                                     newline + newline + newline + newline + _emailSing;

                using (MailMessage mailMessage = new MailMessage())
                {

                    mailMessage.From = new MailAddress("aaron.saunders@anglingdirect.co.uk");
                    mailMessage.Subject = _sub;
                    mailMessage.Body = errortomail;
                    mailMessage.IsBodyHtml = true;
                    mailMessage.To.Add("aaron.saunders@anglingdirect.co.uk");
                    SmtpClient smtp = new SmtpClient
                    {
                        Host = "anglingdirect-co-uk.mail.protection.outlook.com",
                        Port = 25
                    }; // creating object of smptpclient  

                    smtp.Send(mailMessage); //sending Email  

                }
            }
            catch (Exception em)
            {
                em.ToString();

            }
        }

        public static void SendCustomTomail(string title, string body)
        {
            using (MailMessage mailMessage = new MailMessage())
            {

                mailMessage.From = new MailAddress("aaron.saunders@anglingdirect.co.uk");
                mailMessage.Subject = title;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add("aaron.saunders@anglingdirect.co.uk");
                SmtpClient smtp = new SmtpClient
                {
                    Host = "anglingdirect-co-uk.mail.protection.outlook.com",
                    Port = 25
                }; // creating object of smptpclient  

                smtp.Send(mailMessage); //sending Email  
            }
        }
    }
}
