﻿using Coypu;
using Coypu.Drivers;
using Coypu.Drivers.Selenium;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;


namespace ClockingInDataBase
{
    class Program
    {
        static DataTable _employeetable = new DataTable();
        static DataTable _rawperiods = new DataTable();
        static DataTable _rawclockings = new DataTable();
        static DataTable _timesheets = new DataTable();
        static DataTable _newrawclockings = new DataTable();
        static DataTable _newtimesheets = new DataTable();
        static DataTable _newrawperiods = new DataTable();
        static DataTable _rawclockingscount = new DataTable();
        public static int NextTimeSheetNumber = 0;
        public static int Timesheetid;
        public static Int64 Periodid;
        public static string Connstring = @"Data Source=AD-SVR0005\ATSM;Initial Catalog=ATSM;Integrated Security=True";
        public static string[] Lines;
        public static DataTable Linestoremove;
        public static DataTable Alllines;
        public static DataTable Linestokeep = new DataTable();      
        static void Main()
        {
            LoadClockings(true, "-2"); // string used for how many days of clockings you want to load from clocking machine
                                       // true to stop excessive sql writing
        }
        public static void LoadClockings(bool elimexisting, string negdays)
        {
            ColumnsBuilder();
            LoadEmployees();
            LoadTimeSheets(negdays);
            DownloadDat();
            ReadDat();
            DeleteDat();
            LoadRawClockings(negdays);
            if (elimexisting) EliminateExistingRaws(); // Used to reduce SQL writing, if you bool false you will re-update all raw periods first and last clocking ID's for every line in the dat read
            int amt = Lines.Length;
            if (amt == 0) return;
            foreach (string line in Lines)
            {
                string[] linesplit = line.Split('\t');
                int userId = Convert.ToInt32(linesplit[0]);
                if (userId == 2) continue;
                string badgenumber = linesplit[0];
                string userName = linesplit[1];
                DateTime date = Convert.ToDateTime(linesplit[2]);
                int mostly2 = Convert.ToInt32(linesplit[3]);
                int checkState = Convert.ToInt32(linesplit[4]);
                int employeesuperId =
                (from DataRow rowt in _employeetable.Rows
                    where linesplit[0] == rowt["BadgeNumber"].ToString()
                    select Convert.ToInt32(rowt["SuperID"])).FirstOrDefault();
                int timesheetid = CheckTimeSheets(employeesuperId, date);
                if (timesheetid == 0)
                {
                    NewTimeSheetBuilder(GetNextTimeSheetNumber(), line);
                    WriteTimeSheets();
                    LoadTimeSheets(negdays);
                    timesheetid = CheckTimeSheets(employeesuperId, date);
                }
                LoadRawPeriods();
                long periodid = CheckRawPeriods(timesheetid);
                if (periodid == 0)
                {
                    NewRawPeriods(userId);
                    WriteRawPeriods();
                    LoadRawPeriods();

                    periodid = CheckRawPeriods(timesheetid);
                }
                if (periodid == 0)
                {
                    NewRawPeriods(userId);
                    WriteRawPeriods();
                    LoadRawPeriods();
                    periodid = CheckRawPeriods(timesheetid);
                    if (periodid == 0)
                    {
                        throw new Exception("Still doesn't work");
                    }

                }
                else
                {
                    int rawclocknum = CheckRawClocking(date, badgenumber);
                    if (rawclocknum == 0)
                    {
                        NewRawClockingsBuilder(userId, userName, date, mostly2, checkState);
                        WriteRawClockings();
                    }
                    LoadRawClockings(negdays);
                    int rawclockamt = CountClockingIDsforDay(badgenumber, date);
                    switch (rawclockamt)
                    {
                        case 0:
                            throw new Exception("Can't find any raw clockings");
                        case 1:
                            WriteStartClockingIdRawPeriod(CheckRawClocking(date, badgenumber));
                            break;
                        default:
                            DataView dv = _rawclockingscount.DefaultView;
                            dv.Sort = "AdjustedTime asc";
                            DataTable sortedDt = dv.ToTable();
                            WriteStartClockingIdRawPeriod(Convert.ToInt64(sortedDt.Rows[0][0]));
                            WriteEndClockingIdRawPeriod(Convert.ToInt64(sortedDt.Rows[sortedDt.Rows.Count - 1][0]));
                            break;
                    }
                }
            }
        }
        public static void DownloadDat()
        {
            BrowserSession browser = new BrowserSession(new SessionConfiguration()
            {
                AppHost = "http://192.168.100.150/",
                Browser = Browser.Chrome,
                Driver = typeof(SeleniumWebDriver)
            });
            browser.Visit("/csl/login");
            browser.FindField("username").SendKeys("3");
            browser.FindField("userpwd").SendKeys("1234");
            browser.ClickButton("Login");
            browser.Visit("/csl/download");
            browser.ExecuteScript("document.forms[0].sdate.value=ytoday_s;");
            browser.ClickButton("Download");
            Thread.Sleep(3000);
            browser.Dispose();
        }
        public static void LocalClockInGenerate() // Add this to generate a clock time for the currently logged in user
        {
            string name = Environment.UserName;
            string[] split = name.Split('.');
            foreach (DataRow row in _employeetable.Rows)
            {
                if (row[1].ToString().ToUpper() != split[1].ToUpper() ||
                    row[2].ToString().ToUpper() != split[0].ToUpper()) continue;
                string linenew = row[5] + "\t" + split[0] + " " + split[1] + "\t" + DateTime.Now + "\t" + "2"+"\t" + "0";
                Lines = new string[1];
                Lines[0] = linenew;
                break;
            }
        }
        public static void EliminateExistingRaws()
        {
            foreach(string line in Lines)
            {
                string[] linesplit = line.Split('\t');
               Alllines.Rows.Add(linesplit);
            }
            foreach (DataRow row2 in Alllines.Rows)
            {
                bool dontaddtokeep = false;
                foreach (DataRow row in _rawclockings.Rows)
                {
                    string userid = row[3].ToString();
                    string userid2 = row2[0].ToString();
                    string datetime = row[1].ToString();
                    string datetime2 = row2[2].ToString();
                    if(userid == userid2 & datetime == datetime2)
                    {
                        dontaddtokeep = true;                        
                    }
                }
                if (dontaddtokeep) continue;
                Linestokeep.ImportRow(row2);
            }
            Alllines.AcceptChanges();
            string[] lines2 = new string[Linestokeep.Rows.Count];
            int num = 0;
            foreach (DataRow row in Linestokeep.Rows)
            {
                string datarowtoline = row[0] + "\t" + row[1] + "\t" + row[2] + "\t" + row[3] + "\t" + row[4];
                lines2[num] = datarowtoline;
                num++;
            }
            Lines = lines2;
        }
        public static int CountClockingIDsforDay(string badgeNumber, DateTime clockTime)
        {
            _rawclockingscount.Clear();
            DateTime ddate = clockTime.AddHours(-clockTime.Hour).AddMinutes(-clockTime.Minute).AddSeconds(-clockTime.Second);
            EnumerableRowCollection<DataRow> filtered = _rawclockings.AsEnumerable().Where(r =>
                r.Field<DateTime?>("ClockTime").Value.Date.Equals(ddate.Date)
             && r.Field<string>("BadgeNumber").Equals(badgeNumber));
            _rawclockingscount = filtered.CopyToDataTable();
            if(_rawclockingscount.Rows.Count == 0)
                throw new Exception("Row Counting Not Working");
            return _rawclockingscount.Rows.Count;
        }
        public static void ReadDat()
        {
            string path = "C:\\Users\\aaron.saunders.anglingdirect\\Downloads\\";
            DirectoryInfo dir = new DirectoryInfo(path);
            foreach (FileInfo file in dir.EnumerateFiles("attlog*.dat"))
            {               
                Lines = File.ReadAllLines(path + file);
            }
        }
        public static void DeleteDat()
        {
            string path = "C:\\Users\\aaron.saunders.anglingdirect\\Downloads\\";
            DirectoryInfo dir = new DirectoryInfo(path);
            foreach (FileInfo file in dir.EnumerateFiles("attlog*.dat*"))
            {
                file.CopyTo("C:\\Temp\\ATSM\\Backup\\backfile" + DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss") + ".txt",true);
                file.Delete();
            }
        }
        public static void LoadEmployees()
        {
            string query = "select * from ATSMUser.Employees";
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                using (SqlCommand cmd = new SqlCommand(query, sql))
                {
                    sql.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    _employeetable.Clear();
                    da.Fill(_employeetable);
                }
            }
        }
        public static void LoadRawPeriods()
        {
            string query = "select * from ATSMUser.RawPeriods";
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                using (SqlCommand cmd = new SqlCommand(query, sql))
                {
                    sql.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    _rawperiods.Clear();
                    da.Fill(_rawperiods);
                }
            }
        }
        public static void LoadRawClockings(string negdays)
        {
            string query = "select * from ATSMUser.RawClockings WHERE ATSMUser.RawClockings.RetrievalDate > GetDate() "+negdays;
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                using (SqlCommand cmd = new SqlCommand(query, sql))
                {
                    sql.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    _rawclockings.Clear();
                    da.Fill(_rawclockings);
                }
            }
        }
        public static void LoadTimeSheets(string negdays)
        {
            string query = "select * from ATSMUser.TimeSheets WHERE CreatedOn > GetDate() "+negdays;
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                using (SqlCommand cmd = new SqlCommand(query, sql))
                {
                    sql.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    _timesheets.Clear();
                    da.Fill(_timesheets);
                }
            }
        }
        public static int CheckTimeSheets(int userId, DateTime date)
        {
            DateTime ddate = date.AddHours(-date.Hour).AddMinutes(-date.Minute).AddSeconds(-date.Second);
            EnumerableRowCollection<DataRow> filtered = _timesheets.AsEnumerable().Where(r =>
                r.Field<int?>("EmployeeID").Equals(userId) &&
                r.Field<DateTime>("DateWorked").Equals(ddate));
            if (filtered.Any())
            {
                Timesheetid = Convert.ToInt32(filtered.Last()[0]);
                return Convert.ToInt32(filtered.Last()[0]);
            }
            else
            {
                return 0;
            }
        }
        public static int GetNextTimeSheetNumber()
        {
            int num = (int)_timesheets.AsEnumerable().OrderByDescending(r => r[0]).First()[0];
            num++;
            return num;
        }
        public static void NewTimeSheetBuilder(int maxid, string line)
        {
            string[] linesplit = line.Split('\t');
            DataRow tsrow = _newtimesheets.NewRow();
            tsrow[0] = maxid;
            tsrow[1] = 29;
            int employeenumber = 0;
            foreach (DataRow rowt in _employeetable.Rows)
            {
                if (linesplit[0] != rowt["BadgeNumber"].ToString()) continue;
                tsrow[2] = rowt["SuperID"];
                employeenumber = Convert.ToInt32(rowt["SuperID"]);
            }
            DateTime justdate = Convert.ToDateTime(linesplit[2]);
            tsrow[3] = justdate.ToString("yyyy-MM-dd");
            tsrow[4] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
            DateTime linedate = Convert.ToDateTime(linesplit[2]);
            if (CheckTimeSheets(employeenumber, linedate) != 0) return;
            _newtimesheets.Rows.Add(tsrow);
            _timesheets.ImportRow(tsrow);
        }
        public static int CheckRawClocking(DateTime clockTime, string badgeNumber)
        {
            EnumerableRowCollection<DataRow> filtered = _rawclockings.AsEnumerable().Where(r =>
                r.Field<DateTime?>("ClockTime").Equals((DateTime?)clockTime) &&
                r.Field<string>("BadgeNumber").Equals(badgeNumber));
            if (!filtered.Any()) return 0;
            return Convert.ToInt32(filtered.First()[0]) > 0 ? Convert.ToInt32(filtered.First()[0]) : 0;
        }
        public static void NewRawClockingsBuilder(int userId, string userName, DateTime date, int mostly2, int checkState)
        {
            DataRow row = _newrawclockings.NewRow();
            row[1] = Convert.ToDateTime(date.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            row[2] = Periodid;
            row[3] = userId;
            row[4] = Convert.ToDateTime(date.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            row[5] = (short)2;
            row[6] = Convert.ToDateTime(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            if (checkState == 0) { row[7] = "IN"; }
            else { row[7] = "OUT"; }
            row[8] = mostly2;
            row[9] = 0;
            row[10] = checkState;
            bool rowfound = _rawclockings.Rows.Cast<DataRow>().Any(rowc => rowc[1].ToString() == date.ToString() & userId.ToString() == Convert.ToString(rowc[3]) & rowc[2].ToString() != "");
            if (rowfound) return;
            _newrawclockings.Rows.Add(row);
            _rawclockings.ImportRow(row);
        }
        public static long CheckRawPeriods(int timesheetid)
        {
            EnumerableRowCollection<DataRow> filtered = _rawperiods.AsEnumerable().Where(r =>
                r.Field<int>("TimeSheetID").Equals(timesheetid));
            Periodid = 0;
            if (!filtered.Any()) return 0;
            if (Convert.ToInt32(filtered.First()[4]) <= 0) return 0;
            foreach (DataRow row in filtered)
            {
                Periodid = row[0].ToString() != "" ? Convert.ToInt64(row[0].ToString()) : 0;
            }
            return Periodid;
        }
        public static void NewRawPeriods(int badgeNumber)
        {
            DataRow row3 = _newrawperiods.NewRow();
            DataRow employeerow = _employeetable
                .AsEnumerable().SingleOrDefault(r => r.Field<string>("BadgeNumber") == badgeNumber.ToString());
            if (employeerow != null)
            {
                row3[3] = employeerow[0];
                row3[4] = Timesheetid;
                row3[6] = Timesheetid;
                row3[7] = "0";
                _newrawperiods.Rows.Add(row3);
                _rawperiods.ImportRow(row3);
            }
            else
            {
                EmailException.SendCustomTomail("Raw Period Employee Issue", "Probably new employees added");
            }
        }
        public static void WriteTimeSheets()
        {
            using (SqlConnection sql2 = new SqlConnection(Connstring))
            {
                using (SqlBulkCopy t = new SqlBulkCopy(sql2))
                {
                    t.DestinationTableName = "ATSMUser.TimeSheets";
                    if (_newtimesheets.Rows.Count <= 0) return;
                    sql2.Open();
                    t.WriteToServer(_newtimesheets);
                    _newtimesheets.Clear();
                }
            }
        }
        public static void WriteRawPeriods()
        {
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                sql.Open();
                using (SqlBulkCopy s = new SqlBulkCopy(sql))
                {
                    s.DestinationTableName = "ATSMUser.RawPeriods";
                    if (_newrawperiods.Rows.Count <= 0) return;
                    s.WriteToServer(_newrawperiods);
                    _newrawperiods.Clear();
                }
            }
        }
        public static void WriteRawClockings()
        {
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                sql.Open();
                using (SqlBulkCopy s = new SqlBulkCopy(sql))
                {
                    s.DestinationTableName = "ATSMUser.RawClockings";
                    if (_newrawclockings.Rows.Count <= 0) return;
                    s.WriteToServer(_newrawclockings);
                    _newrawclockings.Clear();
                }
            }
        }
        public static void WriteStartClockingIdRawPeriod(Int64 startclockingid)
        {
            if (startclockingid <= 0) return;
            if (Periodid == 0) return;
            string query = "update ATSMUser.RawPeriods set StartClockingID = '" + startclockingid +
                           "' where PeriodID = '" + Periodid + "'";
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                using (SqlCommand cmd = new SqlCommand(query, sql))
                {
                    sql.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void WriteEndClockingIdRawPeriod(Int64 endclockingid)
        {
            if (Periodid == 0) return;
            string query = "update ATSMUser.RawPeriods set EndClockingID = '" + endclockingid + "' where PeriodID = '" + Periodid + "'";
            using (SqlConnection sql = new SqlConnection(Connstring))
            {
                using (SqlCommand cmd = new SqlCommand(query, sql))
                {
                    sql.Open();
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public static void ColumnsBuilder()
        {
            _newtimesheets = new DataTable();
            _newtimesheets.Columns.Add("SuperID");
            _newtimesheets.Columns.Add("SheduleID", typeof(int));
            _newtimesheets.Columns.Add("EmployeeID", typeof(int));
            _newtimesheets.Columns.Add("DateWorked", typeof(DateTime));
            _newtimesheets.Columns.Add("CreatedOn", typeof(DateTime));
            _newrawperiods = new DataTable();
            _newrawperiods.Columns.Add("PeriodID");
            _newrawperiods.Columns.Add("StartClockingID", typeof(int)); // Needs to be the ID in newrawclockings
            _newrawperiods.Columns.Add("EndClocking", typeof(int));
            _newrawperiods.Columns.Add("EmployeeID", typeof(int));
            _newrawperiods.Columns.Add("TimeSheetID", typeof(int));
            _newrawperiods.Columns.Add("StaticTimeClassID");
            _newrawperiods.Columns.Add("ParentID", typeof(int));
            _newrawperiods.Columns.Add("ChildID");
            _newrawclockings = new DataTable();
            _newrawclockings.Columns.Add("Key");
            _newrawclockings.Columns.Add("AdjustedTime", typeof(DateTime)); // Clocking in Time
            _newrawclockings.Columns.Add("PeriodID", typeof(short)); // null 
            _newrawclockings.Columns.Add("userID", typeof(string)); // Badge Number
            _newrawclockings.Columns.Add("Date", typeof(DateTime)); // Clocking In Time
            _newrawclockings.Columns.Add("TerminalID", typeof(int)); // Always 2
            _newrawclockings.Columns.Add("RetrievalDate", typeof(DateTime)); // TimeDate.Now
            _newrawclockings.Columns.Add("InOut", typeof(string)); // Needs to look at checkstate and do 1 = In anything else = out
            _newrawclockings.Columns.Add("Mostly2", typeof(string));  // SupIInfo Column in Raw Clockings
            _newrawclockings.Columns.Add("Status", typeof(short)); // Always 0
            _newrawclockings.Columns.Add("checkState", typeof(string)); // Just add checkstate to notes column
            Linestoremove = new DataTable();
            Linestoremove.Columns.Add("userID", typeof (int));
            Linestoremove.Columns.Add("userName", typeof(string));
            Linestoremove.Columns.Add("Date", typeof(DateTime));
            Linestoremove.Columns.Add("Mostly2", typeof(int));
            Linestoremove.Columns.Add("CheckState", typeof(int));
            Alllines = new DataTable();
            Alllines.Columns.Add("userID", typeof(int));
            Alllines.Columns.Add("userName", typeof(string));
            Alllines.Columns.Add("Date", typeof(DateTime));
            Alllines.Columns.Add("Mostly2", typeof(int));
            Alllines.Columns.Add("CheckState", typeof(int));
            Linestokeep = new DataTable();
            Linestokeep.Columns.Add("userID", typeof(int));
            Linestokeep.Columns.Add("userName", typeof(string));
            Linestokeep.Columns.Add("Date", typeof(DateTime));
            Linestokeep.Columns.Add("Mostly2", typeof(int));
            Linestokeep.Columns.Add("CheckState", typeof(int));
        }
    }
}
            
         
    

