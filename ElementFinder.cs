﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace ClockingInDataBase
{
    public static class ElementFinder
    {
        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds <= 0) return driver.FindElement(by);
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                IWebElement exists = wait.Until(ExpectedConditions.ElementExists(by));
                wait.Until(ExpectedConditions.ElementToBeClickable(exists));
                return exists;
            }
            catch (Exception)
            {
                FindElement(driver, by, timeoutInSeconds);
                //throw;
            }
            return driver.FindElement(by);
        }
    }
}
